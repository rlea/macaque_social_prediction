# macaque_social_prediction

This repository contains stimuli and results associated with the following publication: 

## Social prediction modulates activity of macaque superior temporal cortex 

### Lea Roumazeilles, Matthias Schurz, Mathilde Lojkiewiez, Lennart Verhagen, Urs Schüffelgen, Kevin Marche, Ali Mahmoodi, Andrew Emberton, Kelly Simpson, Olivier Joly, Mehdi Khamassi, Matthew FS. Rushworth, Rogier B. Mars, Jérôme Sallet

Stimuli folder: contains videos (scrambled videos are marked with 's') and pictures for the different tasks.

Contrast_maps folder: contains the group result for the contrasts illustrated in the paper figures. They can be opened in FSLeyes with the accompanying group template.

Resting_state_connectivity folder: contains the maps for the connectivity study. They can be opened in Workbench viewer with the accompanying right surface.
